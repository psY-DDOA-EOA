package bank;

public class Bank {
    private String name;    // naam van de bank
    private int bankno;     // uniek 4-cijferig nummer van de bank 
                            // elk rekeningnummer begint met deze cijfercombinatie
    private int lastAccountno;  // het laatst uitgedeelde rekeningnummer

    /**
     * Initialiseer de velden van deze bank. 
     * @param name De naam van de bank.
     * @param bankno Het unieke nummer van deze bank.
     */
    public Bank (String name, int bankno) {
        this.name = name;
        this.bankno = bankno;
        lastAccountno = 100000 * bankno;
    }

    /**
     * @return Het eerstvolgende nog niet gebruikte rekeningnummer dat begint 
     * met de cijfers van het banknummer.
     */
    public int nextAccountno() {
        lastAccountno++;
        return lastAccountno;
    }
}
