package bank;

public class CurrentAccount extends Account {

	private double creditLimit;

/**
  	 * Initialiseer de velden van deze lopende rekening. 
  	 * Het rekeningnummer wordt gegenereerd en het saldo wordt op 0 gesteld.
  	 * @param b De bank waarbij de lopende rekening wordt geopend.
  	 * @param cust De klant (eigenaar) van deze lopende rekening
  	 * @param credLim De kredietlimiet van deze lopende rekening
  	 */
	public CurrentAccount(Bank b, String cust, double credLim) {
        super(b, cust);
        creditLimit = credLim;
	}

  	/**
  	 * Neem een bedrag op van deze lopende rekening. Als de kredietlimiet wordt 
  	 * overschreden, dan wordt er een OverdrawnException gegeven en gaat de 
 * opname niet door. 
  	 * @param amount Het op te nemen bedrag.
  	 */
	public void withdraw(double amount) throws OverdrawnException {
        if (getBalance() - amount < 0 - getCreditLimit())
            throw new OverdrawnException("not possible to cross the credit limit!");
        else
            super.withdraw(amount);
	}
	  
  	/**
  	 * @return De kredietlimiet van deze lopende rekening.
  	 */
	public double getCreditLimit() {
        return creditLimit;
	}

  	/**
  	 * De kredietlimiet van deze lopende rekening wordt gewijzigd.
  	 * @param credLim De (nieuwe) kredietlimiet van deze lopende rekening.
  	 */
	public void setCreditLimit(double credLim) {
        creditLimit = credLim;
	}
}
