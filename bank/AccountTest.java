package bank;

import java.util.ArrayList;

public class AccountTest {
    public static void main(String[] args) {
        // maak een arraylist en voeg twee lopende en twee spaarrekeningen toe
        Bank rabo = new Bank("Rabobank", 1248);
        ArrayList<Account> accountList = new ArrayList<Account>();
        accountList.add(new CurrentAccount(rabo, "Klant lopend #1", 200D));
        accountList.add(new CurrentAccount(rabo, "Klant lopend #2", 200D));
        accountList.add(new SavingsAccount(rabo, "Klant spaar #1", 0.25D));
        accountList.add(new SavingsAccount(rabo, "Klant spaar #2", 0.25D));
        int i = 1; 
        for (Account account: accountList) {
            // stort bedrag
            account.deposit (100*i);
            i++;

            // toon rekeningnummer en saldo voor opname
            System.out.println("account: " + account.getAccountno());
            System.out.println("balance before withdrawal: " + account.getBalance());

            try { 
                // neem 400 euro op van deze rekening
                account.withdraw(400);
                // toon rekeningnummer en saldo na opname
                System.out.println ("withdrawal successful");
                System.out.println("balance after withdrawal: " + account.getBalance());
            } catch (OverdrawnException e) {
                System.out.println ("withdrawal canceled");
                System.out.println (e.getMessage());
            }

            System.out.println();
        }
    }
}
