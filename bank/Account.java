package bank;

public abstract class Account {
    private long accountno;
    private String customer;
    private double balance;
    private Bank bank;

    /**
     * Initialiseer de velden van deze rekening. 
     * Het rekeningnummer wordt gegenereerd en het saldo wordt op 0 gesteld.
     * @param b De bank waarbij de rekening wordt geopend.
     * @param cust De klant (eigenaar) van deze rekening
     */
    public Account(Bank b, String cust) {
        accountno = b.nextAccountno();
        customer = cust;
    }

    /**
     * Stort een bedrag op deze rekening.
     * @param amount Het te storten bedrag.
     */
    public void deposit(double amount) {
        balance += amount;
    }

    /**
     * Neem een bedrag op van deze rekening.
     * @param amount Het op te nemen bedrag.
     */
    public void withdraw(double amount) throws OverdrawnException {
        balance -= amount;
    }

    /**
     * @return Het nummer van deze rekening.
     */
    public long getAccountno() {
        return accountno;
    }

    /**
     * @return Het saldo van deze rekening.
     */
    public double getBalance() {
        return balance;
    }
}
