package bank;

public class SavingsAccount extends Account {

    private double interestPerc;

    /**
     * Initialiseer de velden van deze spaarrekening. 
     * Het rekeningnummer wordt gegenereerd en het saldo wordt op 0 gesteld.
     * @param b De bank waarbij de spaarrekening wordt geopend.
     * @param cust De klant (eigenaar) van deze spaarrekening
     * @param perc Het rentepercentage van deze spaarrekening
     */
    public SavingsAccount(Bank b, String cust, double perc) {
        super(b, cust);
        interestPerc = perc;
    }

    /**
     * Neem een bedrag op van deze spaarrekening. Als het saldo negatief 
     * dreigt te worden, dan wordt er een OverdrawnException gegeven en gaat
     * de opname niet door. 
     * @param amount Het op te nemen bedrag.
     */
    public void withdraw(double amount) throws OverdrawnException {
        if (getBalance() - amount < 0)
            throw new OverdrawnException("balance too low!");
        else
            super.withdraw(amount);
    }

    /**
     * De rente over de verstreken maand wordt bijgeschreven op deze 
     * spaarrekening.
     */
    public void addInterest() {
        deposit(getBalance() * getInterestPerc() / 100);
    }

    /**
     * @return Het rentepercentage van deze spaarrekening.
     */
    public double getInterestPerc() {
        return interestPerc;
    }

    /**
     * De rentepercentage van deze spaarrekening wordt gewijzigd.
     * @param perc Het (nieuwe) rentepercentage van deze spaarrekening.
     */
    public void setInterestPerc(double perc) {
        interestPerc = perc;
    }
}

